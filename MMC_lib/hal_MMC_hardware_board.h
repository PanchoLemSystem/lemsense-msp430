//----------------------------------------------------------------------------
//  This include file contains definitions specific to the hardware board.
//----------------------------------------------------------------------------
// ********************************************************
//
//  MOSI == SIMO == Master Output -> Slave Input
//  MISO == SOMI == Master Input  -> Slave Output
//
//	----------------------------------------------------------------	---------------------------
//  |          MSP430FR2311       	|	       MSP430FR2000       	|	|			MMC Card   	   |
//  |       -----------------    	|    	-----------------    	|	|      -----------------   |
//  |   /|\|              XIN|-  	|	/|\|              XIN|-  	|	| /|\ |                 |  |
//  |    | |                 |   	|	 | |                 |   	|	|  |  |                 |  |
//  |    --|RST          XOUT|-  	|	 --|RST          XOUT|-  	|	|  |--|Pin4/Vcc         |  |
//  |      |                 |   	|	   |                 |   	|	|     |                 |  |
//  |      |                 |   	|	   |                 |   	|	|     |                 |  |
//  |      |                 |<--	|	   |                 |<--	|	|  <--|Pin6/CD          |  |
//  |      |            P1.4 |-->	|	   |            P1.4 |-->	|	|  -->|Pin1/CS          |  |
//  |      |                 |   	|	   |                 |		|	|     |                 |  |
//  |      |      P1.7/SOMI  |-->	|	   |      P1.7/MOSI  |-->	|	|  -->|Pin7/DOUT (SIMO) |  |
//  |      |      P1.6/SIMO  |<--	|	   |      P1.6/MISO  |<--	|	|  <--|Pin2/DIN  (SOMI) |  |
//  |      |      P1.5/UCLK1 |-->	|	   |      P1.5/UCLK1 |-->	|	|  -->|Pin5/CLK         |  |
//  |      |                 |   	| 	   |                 |   	|	|     |                 |  |
//  |      |                 |   	|	   |                 |   	|	|  |--|Pin3/GND         |  |
//  |       -----------------		|	   	-----------------		|	|	   -----------------   |
//	----------------------------------------------------------------	 --------------------------
//
//  Pin configuration at MSP430F169:	(Cambiar para MSP430FR2311)
//  ----------------------------------------------------------------------------
//   MSP430F169   | MSP Pin	| MSP430FR2000 | MSP Pin | MMC    	  | MMC Pin		|
//  ----------------------------------------------------------------------------
//   P5.0         |	  48    | P1.4     	   |   16	 | ChipSelect |	   1		|
//   P5.2 / SOMI  |   46    | P1.6 / SOMI  |   15	 | DataOut    |    7		|
//   P5.3 / UCLK1 |   47    | P1.5 / UCLK1 |   16	 | Clock      |    5		|
//   P5.6         |   44    | P1.0     	   |   20	 | CardDetect |    6		|
//   P5.1 / SIMO  |   45    | P1.7 / SIMO  |   13	 | DataIn     |    2		|
//                                 					 | GND        |    3 (0 V)	|
//                                 					 | VDD        |    4 (3.3 V)|
//  ----------------------------------------------------------------------------

#include "msp430fr2000.h"            	// Adjust this according to the
                                     	// MSP430 device being used.
// SPI port definitions              	// Adjust the values for the chosen
#define SPI_PxSEL 		P1SEL1     		// interfaces, according to the pin
#define SPI_PxDIR		P1DIR      		// assignments indicated in the
#define SPI_PxIN		P1IN   			// chosen MSP430 device datasheet.
#define SPI_PxOUT		P1OUT	
#define SPI_SIMO		BIT7			// SPI transmit pin		| P1.7
#define SPI_MOSI		SPI_SIMO		// SIMO == MOSI
#define SPI_SOMI		BIT6			// SPI receive pin		| P1.6
#define SPI_MISO		SPI_SOMI		// SOMI == MISO
#define SPI_UCLK		BIT5			// SPI CLK pin 			| P1.5

//----------------------------------------------------------------------------
// SPI/UART port selections.  Select which port will be used for the interface 
//----------------------------------------------------------------------------
#define SPI_SER_INTF	SER_INTF_BRIDGE		// Interface to MMC


// SPI port definitions              		// Adjust the values for the chosen
#define MMC_PxSEL 		SPI_PxSEL      		// interfaces, according to the pin
#define MMC_PxDIR		SPI_PxDIR      		// assignments indicated in the
#define MMC_PxIN		SPI_PxIN       		// chosen MSP430 device datasheet.
#define MMC_PxOUT		SPI_PxOUT      
#define MMC_SIMO		SPI_SIMO
#define MMC_SOMI		SPI_SOMI
#define MMC_UCLK		SPI_UCLK

// Chip Select
#define MMC_CS_PxOUT      P1OUT
#define MMC_CS_PxDIR      P1DIR
#define MMC_CS            BIT4				// P1_4

// Card Detect
#define MMC_CD_PxIN       P1IN
#define MMC_CD_PxDIR      P1DIR
#define MMC_CD            BIT0				// P1_0

#define CS_LOW()    MMC_CS_PxOUT &= ~MMC_CS               			// Card Select
#define CS_HIGH()   while(!halSPITXDONE); MMC_CS_PxOUT |= MMC_CS  	// Card Deselect

#define DUMMY_CHAR 0xFF
