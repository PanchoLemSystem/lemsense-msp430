// *************************************************************************************
//
// Filename:  hal_SPI.h: 
// Declarations for Communication with the MMC (see mmc.c) in unprotected SPI mode.
//
// Version 1.1
//    added ul declaration in macros mmcWriteSector and mmcReadSector
// *************************************************************************************

#ifndef _SPILIB_H
#define _SPILIB_H

#include "hal_hardware_board.h"

//----------------------------------------------------------------------------
//  These constants are used to identify the chosen SPI and UART
//  interfaces.
//----------------------------------------------------------------------------
#define SER_INTF_NULL    0
#define SER_INTF_USCIA0  1
#define SER_INTF_USCIA1  2
#define SER_INTF_BRIDGE	 3

#if SPI_SER_INTF == SER_INTF_USCIA0
 #define halSPIRXBUF  		U0RXBUF
 #define halSPI_SEND(x) 	U0TXBUF=x
 #define halSPITXREADY  	(IFG1&UTXIFG0)         	/* Wait for TX to be ready */
 #define halSPITXDONE  		(UCA0STAT&UCBUSY)       /* Wait for TX to finish */
 #define halSPIRXREADY 		(IFG1&URXIFG0)          /* Wait for TX to be ready */
 #define halSPIRXFG_CLR 	IFG1 &= ~URXIFG0
 #define halSPI_PxIN  		SPI_USART0_PxIN
 #define halSPI_SOMI  		SPI_USART0_SOMI

#elif SPI_SER_INTF == SER_INTF_USCIA1
 #define halSPIRXBUF  U0RXBUF
 #define halSPI_SEND(x) U0TXBUF=x
 #define halSPITXREADY  (IFG1&UTXIFG0)        	 	/* Wait for TX to be ready */
 #define halSPITXDONE  (UCA1STAT&UCBUSY)       		/* Wait for TX to finish */
 #define halSPIRXREADY (IFG1&URXIFG0)          		/* Wait for TX to be ready */
 #define halSPIRXFG_CLR IFG1 &= ~URXIFG0
 #define halSPI_PxIN  SPI_USART0_PxIN
 #define halSPI_SOMI  SPI_USART0_SOMI

#elif SPI_SER_INTF == SER_INTF_BRIDGE
 #define halSPIRXBUF  		UCA0RXBUF
 #define halSPI_SEND(x) 	UCA0RXBUF=x
 #define halSPITXREADY  	(UCA0IFG&UCTXIFG)      	/* Wait for TX to be ready */
 #define halSPITXDONE  		(UCA0STATW&UCBUSY)     	/* Wait for TX to finish */
 #define halSPIRXREADY 		(UCA0IFG&UCRXIFG)    	/* Wait for TX to be ready */
 #define halSPIRXFG_CLR 	IFG1 &= ~URXIFG0
 #define halSPI_PxIN  		SPI_USART0_PxIN
 #define halSPI_SOMI  		SPI_USART0_SOMI
#endif


// Varialbes


// Function Prototypes
void halSPISetup (void);
unsigned char spiSendByte(const unsigned char data);
unsigned char spiReadFrame(unsigned char* pBuffer, unsigned int size);
unsigned char spiSendFrame(unsigned char* pBuffer, unsigned int size);


#endif /* _SPILIB_H */
