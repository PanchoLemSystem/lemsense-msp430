//----------------------------------------------------------------------------
//  This file contains functions that allow the MSP430 device to access the
//  SPI interface.  There are multiple instances of each function; 
//  the one to be compiled is selected by the system variable
//  SPI_SER_INTF, defined in "hal_hardware_board.h".
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
//  void halSPISetup(void)
//
//  DESCRIPTION:
//  Configures the assigned interface to function as a SPI port and
//  initializes it.
//----------------------------------------------------------------------------
//  void halSPIWriteReg(char addr, char value)
//
//  DESCRIPTION:
//  Writes "value" to a single configuration register at address "addr".
//----------------------------------------------------------------------------
//  void halSPIWriteBurstReg(char addr, char *buffer, char count)
//
//  DESCRIPTION:
//  Writes values to multiple configuration registers, the first register being
//  at address "addr".  First data byte is at "buffer", and both addr and
//  buffer are incremented sequentially (within the CCxxxx and MSP430,
//  respectively) until "count" writes have been performed.
//----------------------------------------------------------------------------
//  char halSPIReadReg(char addr)
//
//  DESCRIPTION:
//  Reads a single configuration register at address "addr" and returns the
//  value read.
//----------------------------------------------------------------------------
//  void halSPIReadBurstReg(char addr, char *buffer, char count)
//
//  DESCRIPTION:
//  Reads multiple configuration registers, the first register being at address
//  "addr".  Values read are deposited sequentially starting at address
//  "buffer", until "count" registers have been read.
//----------------------------------------------------------------------------
//  char halSPIReadStatus(char addr)
//
//  DESCRIPTION:
//  Special read function for reading status registers.  Reads status register
//  at register "addr" and returns the value read.
//----------------------------------------------------------------------------
//  void halSPIStrobe(char strobe)
//
//  DESCRIPTION:
//  Special write function for writing to command strobe registers.  Writes
//  to the strobe at address "addr".
//----------------------------------------------------------------------------


#ifndef _SPILIB_C
#define _SPILIB_C
//
//---------------------------------------------------------------
#include "hal_SPI.h"
#include "hal_hardware_board.h"

//#define withDMA

#ifndef DUMMY_CHAR
#define DUMMY_CHAR 0xFF
#endif

// SPI port functions
#if SPI_SER_INTF == SER_INTF_USART0

void halSPISetup(void)
{
	UCTL0 = CHAR + SYNC + MM + SWRST;         // 8-bit SPI Master **SWRST**
	UTCTL0 = CKPL + SSEL1 + SSEL0 + STC;      // SMCLK, 3-pin mode
	UBR00 = 0x02;                             // UCLK/2
	UBR10 = 0x00;                             // 0
	UMCTL0 = 0x00;                            // No modulation
	ME1 |= USPIE0;                            // Enable USART0 SPI mode
	UCTL0 &= ~SWRST;                          // Initialize USART state machine
}

#elif SPI_SER_INTF == SER_INTF_USART1

void halSPISetup(void)
{
	UCTL1 = CHAR + SYNC + MM + SWRST;         // 8-bit SPI Master **SWRST**
	UTCTL1 = CKPL + SSEL1 + SSEL0 + STC;      // SMCLK, 3-pin mode
	UBR01 = 0x02;                             // UCLK/2
	UBR11 = 0x00;                             // 0
	UMCTL1 = 0x00;                            // No modulation
	ME2 |= USPIE1;                            // Enable USART1 SPI mode
	UCTL1 &= ~SWRST;                          // Initialize USART state machine
}

#elif SPI_SER_INTF == SER_INTF_USCIA0

void halSPISetup(void)
{
	UCA0CTL0 = UCMST+UCCKPL+UCMSB+UCSYNC;     // 3-pin, 8-bit SPI master
	UCA0CTL1 = UCSSEL_2 + UCSWRST;            // SMCLK
	UCA0BR0 |= 0x02;                          // UCLK/2
	UCA0BR1 = 0;
	UCA0MCTL = 0;
	UCA0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
}

#elif SPI_SER_INTF == SER_INTF_USCIA1

void halSPISetup(void)
{
	UCA1CTL0 = UCMST+UCCKPL+UCMSB+UCSYNC;     // 3-pin, 8-bit SPI master
	UCA1CTL1 = UCSSEL_2 + UCSWRST;            // SMCLK
	UCA1BR0 |= 0x02;                          // UCLK/2
	UCA1BR1 = 0;
	UCA1MCTL = 0;
	UCA1CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
}

#elif SPI_SER_INTF == SER_INTF_USCIB0

void halSPISetup(void)
{
	UCB0CTL0 = UCMST+UCCKPL+UCMSB+UCSYNC;     // 3-pin, 8-bit SPI master
	UCB0CTL1 = UCSSEL_2+UCSWRST;              // SMCLK
	UCB0BR0 |= 0x02;                          // UCLK/2
	UCB0BR1 = 0;
	//UCB0MCTL = 0;
	UCB0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
}

#elif SPI_SER_INTF == SER_INTF_USCIB1

void halSPISetup(void)
{
	UCB1CTL0 = UCMST+UCCKPL+UCMSB+UCSYNC;     // 3-pin, 8-bit SPI master
	UCB1CTL1 = UCSSEL_2+UCSWRST;              // SMCLK
	UCB1BR0 |= 0x02;                          // UCLK/2
	UCB1BR1 = 0;
	UCB1MCTL = 0;
	UCB1CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
}

#elif SPI_SER_INTF == SER_INTF_USI

void halSPISetup(void)
{
	USICTL0  = USIPE7+USIPE6+USIPE5+USIMST+USIOE+UCSWRST; 	// Port, SPI master
	USICKCTL = USISSEL_2 + USICKPL;           				// SCLK = SMCLK
	USICTL0 &= ~USISWRST;                     				// USI released for operation

	USISRL = 0x00;          		// Ensure SDO low instead of high,
	USICNT = 1;                     // to avoid conflict with CCxxxx
}

#elif SPI_SER_INTF == SER_INTF_BRIDGE

void halSPISetup(void)
{
	UCA0CTLW0 = UCSWRST | UCSTEM | UCSSEL__ACLK | UCMODE0 | UCMST | UCSYNC;	
	// UCSWRST: Software reset enabled
	// UCSTEM: STE pin is used to generate the enable signal for a 4-wire slave
	// UCSSEL__ACLK: eUSCI clock source select. Device specific.
	// UCMODE0: 4-pin SPI with UCxSTE active high: Slave enabled when UCxSTE = 1
	// UCMST: 1b = Master mode
	// UCSYNC: Synchronous mode enabled
	
	UCA0BRW    = 0x0002;        	// Bit rate clock = SMCLK/2 = 8 MHz
	
	UCA0CTLW0 &= ~UCSWRST;          	// Release eUSCI_A UART module for operation
    UCA0IE 	   = UCRXIE;                // Enable eUSCI_A RX interrupt
	
	return;
}

#endif


//Send one byte via SPI
unsigned char spiSendByte(const unsigned char data)
{
	while (halSPITXREADY ==0);    // wait while not ready for TX
	
	halSPI_SEND(data);            // write
	
	while (halSPIRXREADY ==0);    // wait for RX buffer (full)
	
	return (halSPIRXBUF);
}


//Read a frame of bytes via SPI
unsigned char spiReadFrame(unsigned char* pBuffer, unsigned int size)
{
	unsigned long i = 0;
	
	// clock the actual data transfer and receive the bytes; spi_read automatically finds the Data Block
	for (i = 0; i < size; i++)
	{
		while (halSPITXREADY ==0);   // wait while not ready for TX
		
		halSPI_SEND(DUMMY_CHAR);     // dummy write
		
		while (halSPIRXREADY ==0);   // wait for RX buffer (full)
		
		pBuffer[i] = halSPIRXBUF;
	}

	return(0);
}


//Send a frame of bytes via SPI
unsigned char spiSendFrame(unsigned char* pBuffer, unsigned int size)
{
	unsigned long i = 0;
	
	// clock the actual data transfer and receive the bytes; spi_read automatically finds the Data Block
	for (i = 0; i < size; i++)
	{
		while (halSPITXREADY ==0);   // wait while not ready for TX
		
		halSPI_SEND(pBuffer[i]);     // write
		
		while (halSPIRXREADY ==0);   // wait for RX buffer (full)
		
		pBuffer[i] = halSPIRXBUF;
	}
	
	return(0);
}


#ifdef withDMA
#ifdef __IAR_SYSTEMS_ICC__
#if __VER__ < 200
interrupt[DACDMA_VECTOR] void DMA_isr(void)
#else
#pragma vector = DACDMA_VECTOR
__interrupt void DMA_isr(void)
#endif
#endif

#ifdef __CROSSWORKS__
void DMA_isr(void)   __interrupt[DACDMA_VECTOR]
#endif

#ifdef __TI_COMPILER_VERSION__
__interrupt void DMA_isr(void);
DMA_ISR(DMA_isr)
__interrupt void DMA_isr(void)
#endif
{
  DMA0CTL &= ~(DMAIFG);
  LPM3_EXIT;
}
#endif


//---------------------------------------------------------------------
#endif /* _SPILIB_C */
