/***************
 * Description
 */
/*
 *  ACLK = default REFO ~32768Hz, MCLK = SMCLK = 16MHz
 *
 *                               MSP430FR2111
 *                           -------------------
 *                       /|\|                   |
 *                        | |                   |
 *                        --|RST                |
 *                          |                   |
 *               SPI CLK <--|P1.5/UCA0CLK       |
 *                          |                   |
 *              SPI MISO -->|P1.6/UCA0SOMI  P2.0|--> SW UART TX Out
 *                          |                   |
 *              SPI MOSI <--|P1.7/UCA0SIMO  P2.1|<-- SW UART RX In
 *                          |                   |
 *                          |                   |
 */

/***************
 * Includes
 */
#include <msp430.h>
#include "config.h"
#include "diskio.h"
#include "pff.h"
#include "spi.h"

/***************
 * Global Variables
 */
FRESULT pff_result;			// Resultado de funciones pff



int main( void )
{
  // Stop watchdog timer to prevent time out reset
  WDTCTL = WDTPW + WDTHOLD;
  
  	SoftSerial_init(void) 
	  
	spi_initialize

  return 0;
}
