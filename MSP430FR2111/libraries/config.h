#ifndef CONFIG_H_
#define CONFIG_H_

/**
 * F_CPU - To obtain the best results select a UART friendly speed.
 *         Measure the SMCLK speed at P1.4 using a multimeter or oscilloscope.
 *         Verify you are getting your desired frequency. Also, you
 *         will have the best luck if you choose BAUD_RATE and F_CPU
 *         combination that allows the ISR routines to complete before
 *         the next data bit arrives. If you are having problems with
 *         a specific baud rate, use a larger F_CPU value until it
 *         works.  You can test how well it works by use a copy paste
 *         command in your terminal emulator.  That seems to stress
 *         the full duplex aspects of the code.
 *
 *   More info about UART friendly F_CPU and BAUD_RATE combos can be found here:
 *
 *         http://mspgcc.sourceforge.net/baudrate.html
 *         http://www.wormfood.net/avrbaudcalc.php and
 */

#define F_CPU 16000000      // fastest clock
#define BAUD_RATE 9600      // launchpad max speed is 9600. However an FT232RL can go faster
                            // http://www.sparkfun.com/products/718 - FT232RL Breakout Board
#define RX_BUFFER_SIZE 16   // Set the size of the ring buffer data needs to be a power of 2

/***************
 * Definitions
 */
// Pins UART via software (aka UART_SW)
#define UART_SW_TX_PIN 		BIT0   		// SW UART transmit pin	| P2.0 | TB1.1	
#define UART_SW_RX_PIN   	BIT1  		// SW UART receive pin	| P2.1 | TB1.2

// Parameters of UART_SW
#define SW_WHOLE_BIT		1666		// Full bit time [clock-cycles]
										// SW_WHOLE_BIT = SMCLK/UART_SW baudrate
										// 				= 16MHz/9600 = 1666.6

#define SW_HALF_BIT			833			// Half bit time [clock-cycles]

#define SW_TOTAL_BITS		9			// 1 start bit, 8 data bits, 1 stop bit

// Pins SD card
#define SD_ENABLE_PIN		BIT3		// SD enable power		| P1.3


#define TIME_SEND_SPI		16000		// Time to send a SPI data, after the buffer is ready
										// TIME_SEND_SPI = milliseconds/SMCLK
										// 				 = 1ms/16MHz = 16000

/***************
 * States
 */
#define STATE_INIT			0x0000
#define STATE_SPI_RX		0x0001
#define STATE_SPI_TX		0x0002
#define STATE_UART_SW_RX	0x0010
#define STATE_UART_SW_TX	0x0020

#endif
